
import java.util.*;
import java.net.*;
import java.io.*;
import java.sql.*;

public class Servidor {

    /*************************** MAIN *****************************************/
    /**************************************************************************/
    public static void main(String[] args) {
        if (args.length == 0)
            new Servidor();
        else if (args.length == 1) // indica el puerto
            new Servidor(args[0]);
        else
            System.out.println("Cantidad de parametros incorrectos!");
    }
    /**************************************************************************/
    /**************************************************************************/

    private ServerSocket servidor          = null;
    private Socket       sCli              = null;
    private Integer      puerto            = null;
    private TDAMensaje   OMC               = null;
    private ObjectInputStream flujoEntrada = null;
    private DataOutputStream  flujoSalida  = null;

    private Connection cnxPostgres         = null;
    private Connection cnxMySql            = null;

    /** 
    * Constructores sobrecargados
    */
    public Servidor() {
        this("5050"); // puerto 5050 por defecto
    }

    public Servidor(String puerto) {
        try {
            this.puerto = Integer.parseInt(puerto);
        } catch(NumberFormatException nfe) {
            System.err.println(puerto + " debe ser numero entero");
            System.exit(0);
        }

        /*if (!conectToPostgres()) {
            System.out.println("No pude conectarme al Servidor Postgresql");
            System.exit(0);
        }*/
        /*if (!conectToMySql()) {
            System.out.println("No pude conectarme al Servidor MySql");
            System.exit(0);
        }
        */

         if (conectarBBDD(Util.getValueProperty("nameJDBCPostgresql"),
                          Util.getValueProperty("nameDriverPostgresql"),
                          Util.getValueProperty("ipPostgresql"),
                          Util.getValueProperty("portPostgresql"),
                          Util.getValueProperty("nameBDPostgresql"),
                          Util.getValueProperty("userPostgresql"),
                          Util.getValueProperty("pwdPostgresql") ) ) {
            System.out.println("Conectado a PostgreSQL");
        } else 
            System.out.println("Error al conectar en Postgresql");

         if (conectarBBDD(Util.getValueProperty("nameJDBCMysql"),
                          Util.getValueProperty("nameDriverMysql"),
                          Util.getValueProperty("ipMySql"),
                          Util.getValueProperty("portMySql"),
                          Util.getValueProperty("nameBDMySql"),
                          Util.getValueProperty("userMySql"),
                          Util.getValueProperty("pwdMySql") ) ) {
            System.out.println("Conectado a MySql");
        } else
            System.out.println("Error al conectar en Mysql");

        initServidor();
    }
    //--------------------------------------------------------------------------


    /**
    * Metodo principal que ejecuta un ServerSocket el cual escucha las peticiones
    * de los clientes, los cuales envian querys a ejecutar a servidores postgresql
    * y mysql. Si no hay problemas y el query es correcto se le envia el resultado
    * de dicha consulta al cliente.
    */
    private void initServidor() {
        StringBuffer resultado = new StringBuffer();
        try {
            servidor = new ServerSocket(puerto, 1);

            // ---------------  probando, borrar luego
            Enumeration e = NetworkInterface.getNetworkInterfaces();
            while(e.hasMoreElements()) {
                NetworkInterface n = (NetworkInterface) e.nextElement();
                Enumeration ee = n.getInetAddresses();
                while (ee.hasMoreElements()) {
                    InetAddress i = (InetAddress) ee.nextElement();
                    System.out.println(i.getHostAddress());
                }
            }
            //-----------------------------------------------------------------

            System.out.println("Server Iniciado escuchando puerto : " + puerto);
            capturarCtrlC();
            while( true ) {
                System.out.println("Esperando clientes...");
                sCli = servidor.accept();
                System.out.println("CLIENTE CONECTADO");
                // creando flujos de entrada salida del cliente-servidor
                flujoEntrada = new ObjectInputStream(sCli.getInputStream());
                flujoSalida  = new DataOutputStream(sCli.getOutputStream());
                while ( true ) {
                    // leo el objeto enviado por el cliente
                    OMC = (TDAMensaje)flujoEntrada.readObject();
                    if (OMC.getQuery().equalsIgnoreCase("salir") || OMC.getNameBBDD().equalsIgnoreCase("salir") )
                        break;

                    if ( OMC.getNameBBDD().equals(Util.getValueProperty("nameBDPostgresql")) )
                        resultado.append( mandarQuery(cnxPostgres, OMC.getQuery() ) );

                    /*
                    else if (OMC.getNameBBDD().equalsIgnoreCase("personal") ) // switch a bbdd postgresql
                        resultado.append( mandarQuery(cnxPostgres, OMC.getQuery() ));
                    else if (OMC.getNameBBDD().equalsIgnoreCase("producto") ) // switch a bbdd mysql
                        resultado.append( mandarQuery(cnxMySql, OMC.getQuery() ));
                    else
                        resultado.append( "DB " + OMC.getNameBBDD() + " NO encontrada.");
                    */

                    // envio el resultado de la query al cliente
                    flujoSalida.writeUTF( resultado.toString() );
                    resultado.delete(0, resultado.length());
                }
                System.out.println("CLIENTE DESCONECTADO");
                if (flujoEntrada != null) flujoEntrada.close();
                if (flujoSalida != null)  flujoSalida.close();
                if (sCli != null)         sCli.close();
            }
        } catch(IOException ioe) {
            System.err.println( ioe.getMessage() );
        } catch(ClassNotFoundException cnfe) {
            System.err.println( cnfe.getMessage() );
        } finally {
            cerrarServidor();
        }
    }

    /**
    * Metodo que cierra todos los flujos utilizados y el servidor
    */
    private void cerrarServidor() {
        try {
            if (flujoEntrada != null) flujoEntrada.close();
            if (flujoSalida != null)  flujoSalida.close();
            if (sCli != null)         sCli.close();
            if (cnxPostgres != null)  cnxPostgres.close();
            if (cnxMySql != null)     cnxMySql.close();
            if (servidor != null)     servidor.close();
            System.out.println("SERVIDOR CERRADO");
        } catch(IOException ioe) {
            System.err.println(ioe.getMessage());
        } catch (SQLException e) {
            showSQLException(e);
        }
    }

    /**
    * Metodo que captura cuando el usuario preciona ctrl + c y cierra el servidor
    */
    private void capturarCtrlC() {
        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() {
                System.out.println("\nCtrl + C presionado!");
                cerrarServidor();
            }
        });
    }

    /**
    * Metodo que obtiene una conexion a la bbdd postgres
    */

    private boolean conectarBBDD(String jdbc, String nameDriver, String ip, String puerto,
                               String nameBD, String usuario, String passwd) {
        try {
            // cargo el driver de la bbdd
            Class.forName(nameDriver);
            // obtengo conexion
            cnxPostgres = DriverManager.getConnection(jdbc + 
                                                      ip + ":" + puerto + "/" + nameBD,
                                                      usuario, passwd);
            cnxPostgres.setAutoCommit(false);
            return true;
        } catch (SQLException e) {
            showSQLException(e);
            return false;
        } catch (ClassNotFoundException cnfe) {
            return false;
        }
    }



    /*
    private boolean conectToPostgres() {
        try {
            // cargo el driver de postgresql
            Class.forName("org.postgresql.Driver");
            // establezco info del servidor
            String ip      = Util.getValueProperty("ipPostgresql");
            String puerto  = Util.getValueProperty("portPostgresql");
            String nameBD  = Util.getValueProperty("nameBDPostgresql");
            String usuario = Util.getValueProperty("userPostgresql");
            String passwd  = Util.getValueProperty("pwdPostgresql");
            // obtengo conexion
            cnxPostgres = DriverManager.getConnection("jdbc:postgresql://" + 
                                                      ip + ":" + puerto + "/" + nameBD,
                                                      usuario, passwd);
            cnxPostgres.setAutoCommit(false);
            System.out.println("Conectado a Postgresql OK!");
            return true;
        } catch (SQLException e) {
            showSQLException(e);
            return false;
        } catch (ClassNotFoundException cnfe) {
            System.out.println(cnfe.getMessage());
            return false;
        }
    }
    */

    /**
    * Metodo que obtiene una conexion a la bbdd mysql
    */
    /*
    private boolean conectToMySql() {
        try {
            // cargo el driver de mysql
            Class.forName("com.mysql.jdbc.Driver");
            // establezco info del servidor
            String ip      = Util.getValueProperty("ipMySql");
            String puerto  = Util.getValueProperty("portMySql");
            String nameBD  = Util.getValueProperty("nameBDMySql");
            String usuario = Util.getValueProperty("userMySql");
            String passwd  = Util.getValueProperty("pwdMySql");
            // obtengo conexion
            cnxMySql = DriverManager.getConnection("jdbc:mysql://" + 
                                                      ip + ":" + puerto + "/" + nameBD,
                                                      usuario, passwd);
            cnxMySql.setAutoCommit(false);
            System.out.println("Conectado a MySql OK!");
            return true;
        } catch (SQLException e) {
            showSQLException(e);
            return false;
        } catch (ClassNotFoundException cnfe) {
            System.out.println(cnfe.getMessage());
            return false;
        }
    }
    */

    /**
    * Metodo que le envia al servidor correspondiente la query que solicito el cliente,
    * dicha consulta se almacena en un Buffer y es retornada.
    */
    private String mandarQuery(Connection con, String query) {
        Statement stm  = null;
        ResultSet rs   = null;
        ResultSetMetaData rsmd = null;
        StringBuffer resul     = new StringBuffer();
        int numberOfColumns;
        try {
            stm             = con.createStatement();
            rs              = stm.executeQuery(query);
            rsmd            = rs.getMetaData();
            numberOfColumns = rsmd.getColumnCount();
            while ( rs.next() ) { // por cada fila
                for (int i = 1 ; i <= numberOfColumns ; i++) { // por cada columna
                    resul.append( rs.getString(i) + " ");
                }
                resul.append("\n");
            }
            System.out.println("Query execute OK\n" + query);
            return resul.toString();
        } catch (SQLException e) {
            showSQLException(e);
            return "Error!";
        }
    }

    /**
    * Display an SQLException which has occured in this application.
    */
    public static void showSQLException (SQLException e) {
        // Notice that a SQLException is actually a chain of SQLExceptions,
        // let's not forget to print all of them...
        SQLException next = e;
        while (next != null) {
            System.out.println (next.getMessage ());
            System.out.println ("Error Code: " + next.getErrorCode ());
            System.out.println ("SQL State: " + next.getSQLState ());
            next = next.getNextException ();
        }
    }
}
