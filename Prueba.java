
import java.net.*;
import java.util.*;

public class Prueba {

    public static void main(String[] args) {
        /*try {
            System.out.println("IP :" + InetAddress.getLocalHost().getHostAddress());
        } catch (UnknownHostException uhe){}
        System.out.println(Util.getValueProperty("ipPostgresql"));
        */
        String ip;
        try {
            Enumeration<NetworkInterface> interfaces = NetworkInterface.getNetworkInterfaces();
            while (interfaces.hasMoreElements()) {
                NetworkInterface iface = interfaces.nextElement();
                // filters out 127.0.0.1 and inactive interfaces
                if (iface.isLoopback() || !iface.isUp())
                    continue;

                Enumeration<InetAddress> addresses = iface.getInetAddresses();
                while(addresses.hasMoreElements()) {
                    InetAddress addr = addresses.nextElement();
                    ip = addr.getHostAddress();
                    System.out.println(iface.getDisplayName() + " " + ip);
                }
            }
        } catch (SocketException e) {
            throw new RuntimeException(e);
        }
    }
}
