
import java.net.Socket;
import java.net.InetAddress;

import java.io.IOException;

import java.io.DataInputStream;
import java.io.ObjectOutputStream;

public class Cliente {

    /*************************** MAIN *****************************************/
    /**************************************************************************/
    public static void main(String[] args) {
        if (args.length == 0)
            new Cliente();
        else if (args.length == 1) // indica la ip
            new Cliente(args[0]);
        else if (args.length == 2) // indica la ip y el puerto
            new Cliente(args[0], args[1]);
        else
            System.out.println("Cantidad de parametros incorrectos!");
    }
    /**************************************************************************/
    /**************************************************************************/

    private Socket  socketCliente          = null;
    private String  ipServer               = null;
    private Integer puertoServer           = null;
    private DataInputStream   flujoEntrada = null;
    private ObjectOutputStream flujoSalida = null;

    /**
    * Constructores sobrecargados
    */
    public Cliente() {
        this("localhost");              // ip localhost por defecto
    }

    public Cliente(String ipServer) {
        this(ipServer, "5050");         // puerto 5050 por defecto
    }

    public Cliente(String ipServer, String puertoServer) {
        this.ipServer = ipServer;
        try {
            this.puertoServer = Integer.parseInt(puertoServer);
        } catch(NumberFormatException nfe) {
            System.err.println("Error " + puertoServer + " no es numero entero");
            System.exit(0);
        }
        initCliente();
    }

     /**
     * Inicio cliente el cual le envia peticiones al servidor y recibe los resultados
     */
    private void initCliente() {
        StringBuffer  bbdd = new StringBuffer();
        StringBuffer query = new StringBuffer();
        try {
            // inicio socket y flujos
            socketCliente = new Socket( InetAddress.getByName(this.ipServer), this.puertoServer);
            flujoSalida   = new ObjectOutputStream( socketCliente.getOutputStream() );
            flujoEntrada  = new DataInputStream( socketCliente.getInputStream() );
            capturarCtrlC();
            do {
                flujoSalida.flush();
                bbdd.append( Util.entradaTeclado( "Nombre BBDD  : " ) );
                query.append( Util.entradaTeclado( "Query a ejecutar : ") );
                // clase tipo Serializable la cual se envia al servidor
                TDAMensaje TdaObj = new TDAMensaje();
                TdaObj.setNameBBDD(bbdd.toString());
                TdaObj.setQuery(query.toString());
                bbdd.delete(0, bbdd.length());
                query.delete(0, query.length());
                // se realiza el envio
                flujoSalida.writeObject(TdaObj);
                // recibo del servidor el resultado de la consulta
                System.out.println("RESULTADO CONSULTA : \n" + flujoEntrada.readUTF() );
            } while( !query.equals("salir") );
        } catch(IOException ioe) {
            System.err.println( ioe.getMessage() );
        } finally {
            cerrarCliente();
        }
    }

    /**
    * Metodo que captura cuando el usuario preciona ctrl + c y cierra el servidor
    */
    private void capturarCtrlC() {
        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() {
                try {
                    System.out.println("\nCtrl + C presionado!");
                    TDAMensaje TdaObj = new TDAMensaje();
                    TdaObj.setNameBBDD("salir");
                    flujoSalida.writeObject(TdaObj);
                    cerrarCliente();
                } catch(IOException ioe) {
                    System.err.println( ioe.getMessage() );
                }
            }
        });
    }
    
    /**
    * Metodo que cierra todos los flujos utilizados por el cliente.
    */
    private void cerrarCliente() {
        try {
            if (flujoSalida != null)   flujoSalida.close();
            if (flujoEntrada != null)  flujoEntrada.close();
            if (socketCliente != null) socketCliente.close();
            System.out.println("Flujos cliente Cerrados.");
        } catch(IOException ioe) {
            System.err.println( ioe.getMessage() );
        }
    }
}
