
import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

public class Util {

    public static String entradaTeclado(String msj) {
        BufferedReader bf = new BufferedReader( new InputStreamReader(System.in) );
        System.out.printf(msj);
        try {
            return bf.readLine();
        } catch(IOException ioe) {
            return "";
        }
    }

    public static String getValueProperty(String name) {
        Properties prop = new Properties();
        InputStream input = null;
        try {
            input = new FileInputStream("config.properties");
            // load a properties file
            prop.load(input);
            // get the property value
            return prop.getProperty(name);
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        } finally {
            try {
                if (input != null) input.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

}
