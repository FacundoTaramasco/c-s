
import java.io.Serializable;

public class TDAMensaje implements Serializable {

    private String nameBBDD;
    private String query;

    public TDAMensaje() {
        this("", "");
    }

    public TDAMensaje(String nameBBDD, String query) {
        this.nameBBDD  = nameBBDD;
        this.query = query;
    }

    // Getters
    public String getNameBBDD() {
        return this.nameBBDD;
    }
    public String getQuery() {
        return this.query;
    }

    // Setters
    public void setNameBBDD(String nameBBDD) {
        this.nameBBDD = nameBBDD;
    }
    public void setQuery(String query) {
        this.query = query;
    }

    // Customs
    public String toString() {
        return "(nameBBDD : " + getNameBBDD() + ", Query : " + getQuery() + ")";
    }

}
